'use strict';

// chrome.runtime.onInstalled.addListener(function() {
//   chrome.storage.sync.set({color: 'pink'}, function() {
//     console.log("color,pink");
//   });
//     chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
//       chrome.declarativeContent.onPageChanged.addRules([{
//         conditions: [new chrome.declarativeContent.PageStateMatcher({
//           pageUrl: {hostEquals: 'developer.chrome.com'},
//         })
//         ],
//             actions: [new chrome.declarativeContent.ShowPageAction()]
//       }]);
//     });
// });

chrome.contextMenus.create({
  id: 'json-view',
  title: 'JSON格式化',
  contexts: ['selection']
})
chrome.contextMenus.create({
  id: 'image-base64',
  title: '解析图片为base64',
  contexts: ['image']
})
chrome.contextMenus.create({
  id: 'image-download-default',
  title: 'Download Image',
  contexts: ['image']
})
chrome.contextMenus.create({
  id: 'image-download-png',
  title: 'Download To PNG',
  contexts: ['image']
})
chrome.contextMenus.create({
  id: 'image-download-jpg',
  title: 'Download To JPG',
  contexts: ['image']
})
chrome.contextMenus.create({
  id: 'image-download-gif',
  title: 'Download To GIF',
  contexts: ['image']
})
chrome.contextMenus.create({
  id: 'video-download',
  title: 'Open Video',
  contexts: ['video']
})

chrome.contextMenus.onClicked.addListener(function(info, tab) {
  // 文本生成
  if (info.selectionText) {
    var target_content = ''
    if (info.selectionText) {
      target_content = info.selectionText
    }
    if (/\[|}/.test(target_content)) {
      chrome.tabs.sendMessage(tab.id, { action: 'json-view', target_content }, function(response) {

      })
    }
  }
  // 解析图片为base64
  else if (info.srcUrl && info.menuItemId === 'image-base64') {
    if (info.srcUrl) {
      let url = info.srcUrl.replace(/\?.+/g, '').replace(/=.+/g, '')
      let name = /(\/?|\\?)([^\/\\]+)$/.exec(url) || []
      name = name[2] || ''
      let ext = /(png|jpe?g|gif)$/i.exec(name) || []
      let type = ext[1]
      if (type) {
        type = type.toLowerCase()
        type = type === 'jpg' ? 'jpeg' : type
        type = 'image/' + type
      } else {
        type = 'image/png'
      }
      filer.loadImageToBase64(info.srcUrl, type).then(function(base64) {
        chrome.tabs.create({url: "base64.html?name=" + name + "&code=" + base64})
      }).catch(function(){
        alert('图片获取失败')
      })
    } else {
      alert('图片获取失败')
    }
  }
  // 下载 image-download-png image-download-jpg image-download-gif
  else if (info.srcUrl && /image-download/.test(info.menuItemId)) {
    if (info.srcUrl) {
      downloadImage(info.srcUrl, info.menuItemId)
    } else {
      alert('图片获取失败')
    }
  }
  // 下载 video
  else if (info.srcUrl && /video-download/.test(info.menuItemId)) {
    window.open(info.srcUrl)
  }
})

function downloadImage (srcUrl, did) {
  let url = srcUrl.replace(/\?.+/g, '')
  let name = /(\/?|\\?)([^\/\\]+)$/.exec(url) || []
  name = name[2] || ''
  let ext = /(png|jpe?g|gif|svg)$/i.exec(name) || []
  ext = ext[1]
  if ('image-download-png' === did) {
    ext = 'png'
  } else if ('image-download-jpg' === did) {
    ext = 'jpg'
  } else if ('image-download-gif' === did) {
    ext = 'gif'
  }
  let type = ''
  if (ext) {
    ext = ext.toLowerCase()
    ext = ext === 'jpeg' ? 'jpg' : ext
    type = ext === 'jpg' ? 'jpeg' : ext
    type = 'image/' + type
  } else {
    ext = 'png'
    type = 'image/png'
  }
  if (/svg/.test(ext)) {
    filer.downloadByHref(srcUrl, name)
    return true
  }
  filer.loadImageToBase64(srcUrl, type).then(function(base64) {
    let url = base64
    let fileName = name
    fileName = fileName || Date.now()
    let type = ext
    url = url.replace(/image\/(png|jpe?g|gif|svg\+xml)/, 'image/' + type)
    fileName = fileName.replace(/\.[^\.]+$/, '.' + type)
    if (!/\./.test(fileName)) {
      fileName = fileName + '.' + type
    }
    filer.downloadByBase64(url, fileName)
  }).catch(function(){
    alert('图片获取失败')
  })
}

const filer = {
  /**
   * 创建并下载/导出文件
   * @param  {String} fileName 文件名
   * @param  {String} content  文件内容
   */
  downloadByHref (url, fileName) {
    var a = document.createElement('a')
    a.target = '_blank'
    a.download = fileName || Date.now()
    a.href = url
    document.body.appendChild(a)
    a.click()
    document.body.removeChild(a)
  },
  /**
   * @function 加载图片并转化为base64
  */
  loadImageToBase64 (url, type) {
    return this.loadImage(url).then((img) => {
      var data = this.imageToBase64(img, type)
      return data
    })
  },

  /**
   * @function 加载图片
  */
  loadImage (url) {
    return new Promise((resolve, reject) => {
      var img = document.createElement('img')
      img.onload = function () {
        resolve(img)
      }
      img.onerror = function (e) {
        reject(e)
      }
      img.src = url
      img.crossOrigin= 'anonymous'
    })
  },

  /**
   * @function 图片转base64
  */
  imageToBase64(img, type) {
    var canvas = document.createElement('canvas')
    canvas.width = img.width
    canvas.height = img.height
    var ctx = canvas.getContext('2d')
    if (/jpe?g/.test(type)) {
      ctx.fillStyle = '#ffffff' // 设置填充颜色为白色
      ctx.fillRect(0, 0, canvas.width, canvas.height)
    }
    ctx.drawImage(img, 0, 0, img.width, img.height)
    var dataURL = canvas.toDataURL(type || 'image/png')
    return dataURL
    // return dataURL.replace('data:image/png;base64,', '')
  },
  /**
   * @function 创建并下载/导出文件
   * @param  {String} fileName 文件名
   * @param  {String} content  blob文件内容
   */
  downloadFile (fileName, content) {
    var blob = new Blob([content])
    this.downloadByHref(URL.createObjectURL(blob), fileName)
    URL.revokeObjectURL(blob)
  },

  /**
   * 创建并下载/导出文件
   * @param  {String} fileName 文件名
   * @param  {String} content  文件内容
   */
  downloadByHref (url, fileName) {
    var a = document.createElement('a')
    a.target = '_blank'
    a.download = fileName || Date.now()
    a.href = url
    document.body.appendChild(a)
    a.click()
    document.body.removeChild(a)
  },

  /**
   * 创建并下载/导出文件
   * @param  {String} url  文件内容
   * @param  {String} fileName 文件名
   */
  downloadByBase64 (url, fileName) {
    let reg = /data:(\w+\/\w+);base64,/i
    if (!reg.test(url)) {
      return true
    }
    let text = url
    let mime = reg.exec(text)[1]
    let type = '.txt'
    // 图片类型
    if (/image/i.test(mime)) {
      type = mime.replace(/.+\//, '').toLowerCase()
    } else {
      for (let key in mimeTypes) {
        if (mime.toLowerCase() === mimeTypes[key]) {
          type = key
        }
      }
    }
    type = '.' + type
    type = type.replace('..', '.')
    fileName = !fileName ? Date.now() + type : /\.\w+/i.test(fileName) ? fileName : fileName + type
    this.downloadFile(fileName, this.base64ToBlob(text))
  },

  /**
   * @function base64转化为Blob
  */
  base64ToBlob(dataurl) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n)
    }

    return new Blob([u8arr], { type: mime })
  }
}