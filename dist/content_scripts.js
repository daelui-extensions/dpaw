/**
 * @function 添加元素
*/
function appendElement (tag, attrs, parentEl) {
  return new Promise(function (resolve, reject) {
    try {
      let el = document.createElement(tag)
      el.onload = function () {
        resolve(this, tag, attrs, parentEl)
      }
      for (let key in attrs) {
        el.setAttribute(key, attrs[key])
      }
      parentEl = parentEl || document.body || document.documentElement
      if (/link|script/i.test(tag)) {
        parentEl = document.head || document.body || document.documentElement
      }
      parentEl.appendChild(el)
    } catch (e) {
      reject(e)
    }
  })
}

chrome.runtime.onMessage.addListener(function(msg, sender, sendResponse) {
  if (msg.action === 'json-view') {
    window.$dextensions.toJsonView(msg.target_content)
  }
})

/* 注入脚本 */
setTimeout(function(){
  appendElement('script', {
    src: chrome.extension.getURL('resource/js/eye-dropper.js')
  })
  appendElement('script', {
    src: chrome.extension.getURL('resource/js/copy-kick.js')
  })
  appendElement('script', {
    src: chrome.extension.getURL('resource/js/html2canvas.min.js')
  })
  appendElement('script', {
    src: chrome.extension.getURL('resource/js/imager.js')
  })
  appendElement('script', {
    src: chrome.extension.getURL('resource/js/video.js')
  })

  // 扩展
  appendElement('script', {
    src: chrome.extension.getURL('resource/js/custom.js')
  })
}, 1000)

// JSON格式化
;(function () {
  window.$dextensions = window.$dextensions || {}
  window.$dextensions.toJsonView = function (target_content) {
    let data = parser.toObject(target_content, true)
    let el = document.querySelector('#json_view')
    if (!el) {
      el = document.createElement('div')
      el.id = 'json_view'
      let style = 'overflow:hidden;position:fixed;right:2px;bottom:2px;padding:4px;max-width:80vw;max-height:80vh;background:#fff;z-index:9999;border:1px solid #439eff;border-radius:4px;'
      el.setAttribute('style', style)
      el.innerHTML = '<div style="overflow:auto;max-width:80vw;max-height:80vh;font-size:12px;color:#333;"></div><span style="position:absolute;right:2px;top:2px;font-size:16px;font-weight:700;cursor:pointer;color:#439eff">×</span>'
      document.body.prepend(el)
      el.lastChild.onclick = function () {
        el.style.display = 'none'
      }
    }
    el.style.display = 'block'
    if (data && data !== null && typeof data === 'object') {
      el.firstChild.innerHTML = '<pre>' + JSON.stringify(data, null, '  ') + '</pre>'
    }
  }

  const parser = {
    /**
     * @function 数据转换为对象
     * @param {String} data // 数据字符串
     * @return {Any}
    */
    toObject (data, isDeep) {
      if (typeof data === 'string') {
        let result = null
        // JSON解析
        if (/^\s*\t*(\[|\{)/m.test(data)) {
          try {
            result = JSON.parse(data)
          } catch (e) {
            result = null
          }
        }
        // 函数解析
        if (result === null) {
          try {
            result = new Function('return ' + data)()
          } catch (e) {
            result = null
          }
        }
        // 转义解析
        if (result === null) {
          try {
            result = JSON.parse('{"a":"' + data + '"}')
            result = JSON.parse(result.a)
          } catch (e) {
            result = null
          }
        }
        // 深度解析
        if (Object.prototype.toString.call(result) === '[object Object]' && isDeep) {
          result = this.solveDeep(result)
        }
  
        data = result === null ? data : result
      }
  
      return data
    },
  
    /**
     * @function 解析子级对象
    */
    solveDeep (data) {
      let result = data
      // 数组形式
      if (Array.isArray(data)) {
        result = data.map(item => {
          return this.toObject(item)
        })
      }
      // 对象形式
      else if (Object.prototype.toString.call(data) === '[object Object]') {
        for (let key in data) {
          // JSON格式字符
          if (typeof data[key] === 'string' && /^\s*({|\[)/m.test(data[key])) {
            try {
              data[key] = this.toObject(data[key])
            } catch (e) {}
          }
          // 对象形式
          if (Object.prototype.toString.call(data[key]) === '[object Object]') {
            data[key] = this.solveDeep(data[key])
          }
        }
      }
  
      return result
    }
  }
})();