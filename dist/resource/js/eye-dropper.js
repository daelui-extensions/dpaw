// 取色器
(function(){
  if ('EyeDropper' in window) {
    console.log('取色器可以使用 -> 按下 Ctrl + Shift + E 唤起')
    setTimeout(function() {
      const eyeDropper = new EyeDropper()
      const el = document.body || document.document
      el && el.addEventListener('keydown', async (e) => {
        if (e.ctrlKey && e.shiftKey && e.keyCode === 69) {
          try {
            const result = await eyeDropper.open()
            let rgbValue = result.sRGBHex
            let hexValue = rgbToHex(rgbValue).hex
            if (/[a-f]+/.test(rgbValue)) {
              rgbValue = hexToRgb(rgbValue)
            }
            const res = 'RGB：' + rgbValue + '\nHEX：' + hexValue
            copy(res)
            alert(res)
            console.log(res)
          } catch (e) {
            console.log('用户取消了取色')
          }
        }
      })
    }, 1500)
  }
  // RBG to HEX
  function rgbToHex (rgb) {
    var rRgba = /rgba?\((\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})(\s*,\s*([.\d]+))?\)/
    var r, g, b, a
    var rsa = rgb.replace(/\s+/g, '').match(rRgba)
    if (rsa) {
      r = (+rsa[1]).toString(16)
      r = r.length === 1 ? '0' + r : r
      g = (+rsa[2]).toString(16)
      g = g.length === 1 ? '0' + g : g
      b = (+rsa[3]).toString(16)
      b = b.length === 1 ? '0' + b : b
      a = (+(rsa[5] ? rsa[5] : 1)) * 100
      return {
        hex: '#' + r + g + b,
        alpha: Math.ceil(a)
      }
    } else {
      return {
        hex: rgb,
        alpha: 100
      }
    }
  }
  // HEX to RBG
  function hexToRgb(hex) {    
    var r = parseInt(hex.slice(1,3), 16),
        g = parseInt(hex.slice(3,5), 16),
        b = parseInt(hex.slice(5,7), 16);
    return "rgb(" + r + ", " + g + ", " + b + ")";
  }
  function copy (str) {
    var el = document.createElement('textarea')
    el.value = str
    el.setAttribute('readonly', '')
    el.style.position = 'absolute'
    el.style.left = '-9999px'
    document.body.appendChild(el)
    el.select()
    document.execCommand('copy')
    document.body.removeChild(el)
  }
})();