(function(){
  /**
   * @function 添加元素
  */
  function appendElement (tag, attrs, parentEl) {
      return new Promise(function (resolve, reject) {
        try {
          let el = document.createElement(tag)
          el.onload = function () {
            resolve(this, tag, attrs, parentEl)
          }
          for (let key in attrs) {
            el.setAttribute(key, attrs[key])
          }
          parentEl = parentEl || document.body || document.documentElement
          if (/link|script/i.test(tag)) {
            parentEl = document.head || document.body || document.documentElement
          }
          parentEl.appendChild(el)
        } catch (e) {
          reject(e)
        }
      })
  }

  /* 扩展脚本 */
  // 远程样式
  // appendElement('style', {
  //   type: 'text/css',
  //   href: 'URL'
  // })
  // 远程脚本
  // appendElement('script', {
  //   src: 'URL'
  // })
})();