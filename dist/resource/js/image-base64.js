/**
 * @description 图片转换为base64
*/

var href = location.href
href = href.split('?')[1] || ''
let params = href.split('&')
let map = {}
params.forEach(function(item){
  let kv = /^([^=]+)=(.+)/.exec(item)
  if (kv) {
    map[kv[1]] = kv[2]
  }
})
if (!/\.\w/i.test(map.name)) {
  map.name = Date.now() + '.png'
}
let ext = /(png|jpe?g|gif)$/i.exec(map.name) || []
let type = ext[1]
if (type) {
  type = type.toLowerCase()
  type = type === 'jpeg' ? 'jpg' : type
} else {
  type = 'png'
}
document.querySelector('[name=name]').value = map.name || ''
document.querySelector('[name=base64]').value = map.code || ''
document.querySelector('[name=type]').value = type || 'png'
document.querySelector('[name=download]').addEventListener('click', function(){
  let url = document.querySelector('[name=base64]').value
  let type = document.querySelector('[name=type]').value || 'png'
  const img = new Image()
  let imtype = /jpe?g/.test(type) ? 'image/jpeg' : /gif/.test(type) ? 'image/gif' : 'image/png'
  img.src = url
  img.onload = function () {
    var base64 = filer.imageToBase64(img, imtype)
    let url = base64
    let fileName = document.querySelector('[name=name]').value
    fileName = fileName || Date.now()
    url = url.replace(/image\/(png|jpe?g|gif)/, 'image/' + type)
    fileName = fileName.replace(/\.[^\.]+/, '.' + type)
    if (!/\./.test(fileName)) {
      fileName = fileName + '.' + type
    }
    filer.downloadByBase64(url, fileName)
  }
  img.onerror = function () {
    alert('图片解析失败')
  }
})

const filer = {
  /**
   * @function 图片转base64
  */
  imageToBase64(img, type) {
    var canvas = document.createElement('canvas')
    canvas.width = img.width
    canvas.height = img.height
    var ctx = canvas.getContext('2d')
    if (/jpe?g/.test(type)) {
      ctx.fillStyle = '#ffffff' // 设置填充颜色为白色
      ctx.fillRect(0, 0, canvas.width, canvas.height)
    }
    ctx.drawImage(img, 0, 0, img.width, img.height)
    var dataURL = canvas.toDataURL(type || 'image/png')
    return dataURL
    // return dataURL.replace('data:image/png;base64,', '')
  },

  /**
   * @function 创建并下载/导出文件
   * @param  {String} fileName 文件名
   * @param  {String} content  blob文件内容
   */
  downloadFile (fileName, content) {
    var blob = new Blob([content])
    this.downloadByHref(URL.createObjectURL(blob), fileName)
    URL.revokeObjectURL(blob)
  },

  /**
   * 创建并下载/导出文件
   * @param  {String} fileName 文件名
   * @param  {String} content  文件内容
   */
  downloadByHref (url, fileName) {
    var a = document.createElement('a')
    a.target = '_blank'
    a.download = fileName || Date.now()
    a.href = url
    document.body.appendChild(a)
    a.click()
    document.body.removeChild(a)
  },

  /**
   * 创建并下载/导出文件
   * @param  {String} url  文件内容
   * @param  {String} fileName 文件名
   */
  downloadByBase64 (url, fileName) {
    let reg = /data:(\w+\/\w+);base64,/i
    if (!reg.test(url)) {
      return true
    }
    let text = url
    let mime = reg.exec(text)[1]
    let type = '.txt'
    // 图片类型
    if (/image/i.test(mime)) {
      type = mime.replace(/.+\//, '').toLowerCase()
    } else {
      for (let key in mimeTypes) {
        if (mime.toLowerCase() === mimeTypes[key]) {
          type = key
        }
      }
    }
    type = '.' + type
    type = type.replace('..', '.')
    fileName = !fileName ? Date.now() + type : /\.\w+/i.test(fileName) ? fileName : fileName + type
    this.downloadFile(fileName, this.base64ToBlob(text))
  },

  /**
   * @function base64转化为Blob
  */
  base64ToBlob(dataurl) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n)
    }

    return new Blob([u8arr], { type: mime })
  }
}