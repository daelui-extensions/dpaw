/**
 * @function JSON格式化
*/

;(function(){
  window.$dextensions = window.$dextensions || {}
  window.$dextensions.toJsonView = function (target_content) {
    let data = parser.toObject(target_content, true)
    let el = document.querySelectorAll('pre')
    if (el && el[0] && el.length === 1) {
      el = el[0]
    } else {
      el = document.querySelector('#json_view')
      if (!el) {
        el = document.createElement('div')
        el.id = 'json_view'
        document.body.prepend(el)
      }
    }
    if (data && data !== null && typeof data === 'object') {
      el.innerHTML = JSON.stringify(data, null, '  ')
    }
  }

  const parser = {
    /**
     * @function 数据转换为对象
     * @param {String} data // 数据字符串
     * @return {Any}
    */
    toObject (data, isDeep) {
      if (typeof data === 'string') {
        let result = null
        // JSON解析
        if (/^\s*\t*(\[|\{)/m.test(data)) {
          try {
            result = JSON.parse(data)
          } catch (e) {
            result = null
          }
        }
        // 函数解析
        if (result === null) {
          try {
            result = new Function('return ' + data)()
          } catch (e) {
            result = null
          }
        }
        // 转义解析
        if (result === null) {
          try {
            result = JSON.parse('{"a":"' + data + '"}')
            result = JSON.parse(result.a)
          } catch (e) {
            result = null
          }
        }
        // 深度解析
        if (Object.prototype.toString.call(result) === '[object Object]' && isDeep) {
          result = this.solveDeep(result)
        }

        data = result === null ? data : result
      }

      return data
    },

    /**
     * @function 解析子级对象
    */
    solveDeep (data) {
      let result = data
      // 数组形式
      if (Array.isArray(data)) {
        result = data.map(item => {
          return this.toObject(item)
        })
      }
      // 对象形式
      else if (Object.prototype.toString.call(data) === '[object Object]') {
        for (let key in data) {
          // JSON格式字符
          if (typeof data[key] === 'string' && /^\s*({|\[)/m.test(data[key])) {
            try {
              data[key] = this.toObject(data[key])
            } catch (e) {}
          }
          // 对象形式
          if (Object.prototype.toString.call(data[key]) === '[object Object]') {
            data[key] = this.solveDeep(data[key])
          }
        }
      }

      return result
    }
  }
})();