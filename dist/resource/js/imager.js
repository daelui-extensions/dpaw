/**
 * @description 图片处理
*/

window.$dextensions = window.$dextensions || {}
window.$dextensions.imager = {
  // 生成快照
  convertToCanvas (container, options = {}) {
    // 设置放大倍数
    const scale = window.devicePixelRatio
    // 传入节点原始宽高
    const _width = container.offsetWidth
    const _height = container.offsetHeight
    let { width, height } = options
    width = width || _width
    height = height || _height

    // html2canvas配置项
    const ops = Object.assign({
      scale,
      width,
      height,
      useCORS: true,
      allowTaint: false,
      backgroundColor: null // 设置图片背景为透明
    }, options || {})

    return html2canvas(container, ops)
  },

  dataURLtoBlob(dataurl) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n)
    }

    return new Blob([u8arr], { type: mime })
  },

  // 下载
  downloadToImage (container, options = {}) {
    let me = this
    container = container || document.body
    return this.convertToCanvas(container, options).then(function(canvas){
      let dom = document.body.appendChild(canvas)
      dom.style.display = 'none'
      document.body.removeChild(dom)
      let blob = me.dataURLtoBlob(dom.toDataURL('image/png'))
      let a = document.createElement('a')
      a.setAttribute('href', URL.createObjectURL(blob))
      // 这块是保存图片操作  可以设置保存的图片的信息
      a.setAttribute('download', Date.now() + '.png')
      document.body.appendChild(a)
      a.click()
      URL.revokeObjectURL(blob)
      document.body.removeChild(a)
    })
  }
}

window.$dextensions.checkRun = window.$dextensions.checkRun || function(f, callback, max, current) {
  current = current || 0
  max = max || 10000
  current += 1
  if (current > max) {
    return true
  }
  if (typeof f !== 'function' || typeof callback !== 'function') {
    return true
  }
  try {
    let b = f()
    if (b === true) {
      callback()
      return true
    }
  } catch (e) {}
  setTimeout(function(){
    window.$dextensions.checkRun(f, callback, max, current)
  }, 100)
}

window.$dextensions.checkRun(function(){
  return typeof document === 'object' && !!document.body
}, function(){
  document.body.addEventListener('click', function(){
    let menuEl = document.querySelector('.paw-image-dropdown')
    if (menuEl) {
      menuEl.style.display = 'none'
    }
  })
  document.body.addEventListener('mouseup', function(e){
    setTimeout(function(){
      if (e.button === 2) {
        done()
      }
    }, 100);

    function done () {
      let target = e.target
      let menuEl = document.querySelector('.paw-image-dropdown')
      if (/img/i.test(target.tagName)) {
        window.$dextensions.imager.selectedImg = target
        if (!menuEl) {
          menuEl =appendMenuEl()
        }
        menuEl.style.display = 'block'
        menuEl.style.top = e.clientY + 'px'
        menuEl.style.left = e.clientX + 'px'
      } else {
        if (menuEl) {
          menuEl.style.display = 'none'
        }
      }
  }

    function appendMenuEl () {
      let menuEl = document.createElement('div')
      menuEl.className = 'paw-image-dropdown'
      menuEl.style = 'position:fixed;z-index:99999999;left:0;top:0'
      menuEl.innerHTML = `
        <ul class="paw-image-dropdown-menu">
          <li><a class="item-link" data-id="image-download-default">Download Image</a></li>
          <li><a class="item-link" data-id="image-download-png">Download To PNG</a></li>
          <li><a class="item-link" data-id="image-download-jpg">Download To JPG</a></li>
          <li><a class="item-link" data-id="image-download-gif">Download To GIF</a></li>
        </ul>
        <style type="text/css">
        .paw-image-dropdown {
          padding: 0 8px 8px 0;
        }
        .paw-image-dropdown-menu {
          min-width: 160px;
          padding: 5px 0;
          margin: 2px 0 0;
          font-size: 14px;
          text-align: left;
          list-style: none;
          background-color: #fff;
          background-clip: padding-box;
          border: 1px solid #ccc;
          border: 1px solid rgba(0, 0, 0, .15);
          border-radius: 4px;
          -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
          box-shadow: 0 6px 12px rgba(0,0,0,.175);
        }
        .paw-image-dropdown-menu>li>a {
          display: block;
          padding: 3px 20px;
          clear: both;
          font-weight: 400;
          line-height: 1.42857143;
          color: #333;
          white-space: nowrap;
          cursor: pointer;
          text-decoration: none;
        }
        .paw-image-dropdown-menu>li>a:focus,.paw-image-dropdown-menu>li>a:hover {
          color: #262626;
          text-decoration: none;
          background-color: #f5f5f5
        }
        </style>
      `
      document.body.appendChild(menuEl)
      Array.from(menuEl.querySelectorAll('.item-link')).forEach(function(el){
        el.addEventListener('click', function (e) {
          let did = e.target.getAttribute('data-id')
          let img = window.$dextensions.imager.selectedImg
          downloadImage(img.src, did, img)
        })
      })
      return menuEl
    }

    function downloadImage (srcUrl, did, img) {
      let url = srcUrl.replace(/\?.+/g, '')
      let name = /(\/?|\\?)([^\/\\]+)$/.exec(url) || []
      name = name[2] || ''
      let ext = /(png|jpe?g|gif|svg)$/i.exec(name) || []
      ext = ext[1]
      if ('image-download-png' === did) {
        ext = 'png'
      } else if ('image-download-jpg' === did) {
        ext = 'jpg'
      } else if ('image-download-gif' === did) {
        ext = 'gif'
      }
      let type = ''
      if (ext) {
        ext = ext.toLowerCase()
        ext = ext === 'jpeg' ? 'jpg' : ext
        type = ext === 'jpg' ? 'jpeg' : ext
        type = 'image/' + type
      } else {
        ext = 'png'
        type = 'image/png'
      }
      if (/svg/.test(ext)) {
        filer.downloadByHref(srcUrl, name)
        return true
      }
      filer.loadImageToBase64(srcUrl, type, { img: img }).then(function(base64) {
        let url = base64
        let fileName = name
        fileName = fileName || Date.now()
        let type = ext
        url = url.replace(/image\/(png|jpe?g|gif|svg\+xml)/, 'image/' + type)
        fileName = fileName.replace(/\.[^\.]+$/, '.' + type)
        if (!/\./.test(fileName)) {
          fileName = fileName + '.' + type
        }
        filer.downloadByBase64(url, fileName)
      }).catch(function(){
        alert('图片获取失败')
      })
    }

    const filer = {
      /**
       * 创建并下载/导出文件
       * @param  {String} fileName 文件名
       * @param  {String} content  文件内容
       */
      downloadByHref (url, fileName) {
        var a = document.createElement('a')
        a.target = '_blank'
        a.download = fileName || Date.now()
        a.href = url
        document.body.appendChild(a)
        a.click()
        document.body.removeChild(a)
      },
      /**
       * @function 加载图片并转化为base64
      */
      loadImageToBase64 (url, type) { // , { img } = {}
        return this.loadImage(url).then((img) => {
          var data = this.imageToBase64(img, type)
          return data
        })
      },

      /**
       * @function 加载图片
      */
      loadImage (url) {
        return new Promise((resolve, reject) => {
          var img = document.createElement('img')
          img.onload = function () {
            resolve(img)
          }
          img.onerror = function (e) {
            reject(e)
          }
          img.src = url
          img.crossOrigin= 'anonymous'
        })
      },

      /**
       * @function 图片转base64
      */
      imageToBase64(img, type, { bgColor } = {}) {
        var canvas = document.createElement('canvas')
        canvas.width = img.width
        canvas.height = img.height
        var ctx = canvas.getContext('2d')
        bgColor = (/none|transparent/i.test(bgColor) || !bgColor) ? /jpe?g/.test(type) ? '#ffffff' : '' : bgColor
        if (bgColor) {
          ctx.fillStyle = bgColor // 设置填充颜色为白色
          ctx.fillRect(0, 0, canvas.width, canvas.height)
        }
        ctx.drawImage(img, 0, 0, img.width, img.height)
        var dataURL = canvas.toDataURL(type || 'image/png')
        return dataURL
        // return dataURL.replace('data:image/png;base64,', '')
      },
      /**
       * @function 创建并下载/导出文件
       * @param  {String} fileName 文件名
       * @param  {String} content  blob文件内容
       */
      downloadFile (fileName, content) {
        var blob = new Blob([content])
        this.downloadByHref(URL.createObjectURL(blob), fileName)
        URL.revokeObjectURL(blob)
      },

      /**
       * 创建并下载/导出文件
       * @param  {String} fileName 文件名
       * @param  {String} content  文件内容
       */
      downloadByHref (url, fileName) {
        var a = document.createElement('a')
        a.target = '_blank'
        a.download = fileName || Date.now()
        a.href = url
        document.body.appendChild(a)
        a.click()
        document.body.removeChild(a)
      },

      /**
       * 创建并下载/导出文件
       * @param  {String} url  文件内容
       * @param  {String} fileName 文件名
       */
      downloadByBase64 (url, fileName) {
        let reg = /data:(\w+\/\w+);base64,/i
        if (!reg.test(url)) {
          return true
        }
        let text = url
        let mime = reg.exec(text)[1]
        let type = '.txt'
        // 图片类型
        if (/image/i.test(mime)) {
          type = mime.replace(/.+\//, '').toLowerCase()
        } else {
          for (let key in mimeTypes) {
            if (mime.toLowerCase() === mimeTypes[key]) {
              type = key
            }
          }
        }
        type = '.' + type
        type = type.replace('..', '.')
        fileName = !fileName ? Date.now() + type : /\.\w+/i.test(fileName) ? fileName : fileName + type
        this.downloadFile(fileName, this.base64ToBlob(text))
      },

      /**
       * @function base64转化为Blob
      */
      base64ToBlob(dataurl) {
        var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
          bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while (n--) {
          u8arr[n] = bstr.charCodeAt(n)
        }

        return new Blob([u8arr], { type: mime })
      }
    }

    return true
  })
})

console.log('元素转为图片并下载 -> 控制台选择元素，执行：$dextensions.imager.downloadToImage($0)')
