/**
 * @function 添加元素
*/
function appendElement (tag, attrs, parentEl) {
  return new Promise(function (resolve, reject) {
    try {
      let el = document.createElement(tag)
      el.onload = function () {
        resolve(this, tag, attrs, parentEl)
      }
      for (let key in attrs) {
        el.setAttribute(key, attrs[key])
      }
      parentEl = parentEl || getDocEl()
      if (/link|script/i.test(tag)) {
        parentEl = document.head || getDocEl()
      }
      parentEl.appendChild(el)
    } catch (e) {
      reject(e)
    }
  })
}

// 获取文档元素
function getDocEl () {
  return document.body || document.documentElement
}

window.appendElement = appendElement

/* 注入样式 */
// 代码元素内容可选中
appendElement('style', {
  class: 'copy-kick',
  type: 'text/css'
}).then(function(link){
  link.innerHTML = 'pre,code,.code,.jb51code,div pre,div code,div .code,div .jb51code {' +
    '-webkit-touch-callout: auto !important;' +
    '-webkit-user-select: auto !important;' +
    '-khtml-user-select: auto !important;' +
    '-moz-user-select: auto !important;' +
    '-ms-user-select: auto !important;' +
    'user-select: auto !important;' +
  '}' +
  '.passport-login-container {display: none !important;}' +
  '.arc-body-main,.article_content{height: auto !important;padding-bottom: 200px;}' +
  '#content_views{user-select: auto!important;}#webpack-dev-server-client-overlay{display:none}'
})

// 防止copy注入其它文本
getDocEl().addEventListener('copy', function(e) {
  let s = window.getSelection().toString()
	if (s.length > 0) {
    setTimeout(function(){
      if (e.clipboardData) {
        e.clipboardData.setData('text/plain', s)
      } else if (window.clipboardData) {
        window.clipboardData.setData('text', s)
      }
      console.log(s)
    }, 500)
	}
})
if (window.$ && $('#content_views')) {
  $('#content_views').unbind('copy')
}
if (window.$ && $('.article-content-wrap')) {
  $('.article-content-wrap').unbind('copy')
}
if (window.$ && $('#result')) {
  $('#result').unbind('copy')
}

function copy(t, e) {
  var n = document.createElement("textarea");
  n.readOnly = "readonly",
  n.style.position = "absolute",
  n.style.left = "-9999px",
  n.value = t,
  document.body.appendChild(n),
  n.select(),
  n.setSelectionRange(0, n.value.length),
  document.execCommand("Copy"),
  document.body.removeChild(n),
  e && "[object Function]" === Object.prototype.toString.call(e) && e()
}

