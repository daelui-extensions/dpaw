# DPaw


#### 介绍
- DPaw(Dog Paw) - 小狗爪 页面小助手，浏览器插件模板，可个性化开发
- 取色器(https页面或本地页面可用)
- 辅助代码文本内容可选中，过滤选中文本注入
- DOM生成图片(元素转为图片并下载)
- JSON格式化


#### 软件架构
- HTML + JavaScript


#### 安装步骤
1. 浏览器进入：扩展程序管理，chrome浏览器直接在地址栏输入：chrome://extensions/(Edge浏览器: edge://extensions/)
2. 开启"开发者模式"(Edge浏览器: 开发人员模式)
2. 点击"加载已解压的扩展程序"，打开dist目录


#### 使用方法
- 取色器 -> 快捷键：Ctrl + Shift + E
- DOM生成图片 -> 1、F12打开控制台，2、元素选择器选中需要生成图片的元素，3：命令行执行：`$dextensions.imager.downloadToImage($0)`
- JSON格式化 -> 选中文本，右键菜单选择JSON格式化


#### 个性化
- 可自助修改 dist/resource/js/custom.js内容，添加需要引入的样式或脚本